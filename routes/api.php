<?php

use App\Http\Controllers\Api\First\AuthController;
use App\Http\Controllers\Api\First\CardPayController;
use App\Http\Controllers\Api\First\TransactionController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function () {
    Route::post('register', [ AuthController::class, 'register' ]);
    Route::post('login', [ AuthController::class, 'login' ]);

    Route::group([ 'middleware' => 'auth:api' ], function () {
        Route::post('logout', [ AuthController::class, 'logout' ]);
        Route::post('refresh',[ AuthController::class,'refresh' ]);
        Route::resource('card', CardPayController::class);
        Route::resource('transaction', TransactionController::class);
        Route::post('add-credit',[ TransactionController::class,'addCredit']);
    });
});


