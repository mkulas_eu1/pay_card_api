<?php

namespace App\Rules;

use App\Services\Model\CardPayService;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Lang;

/**
 * @property $from
 */
class Amount implements Rule
{
    public function __construct($from)
    {
        $this->from = $from;
    }

    public function passes($attribute, $value): bool
    {
        $data = CardPayService::cardUuId($this->from)->first();

        if ( $value <=  Crypt::decrypt( $data->money )){
            return true;
        }

        return false;
    }


    public function message(): string
    {
        return Lang::get('validate.not_credit');
    }
}
