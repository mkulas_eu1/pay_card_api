<?php declare(strict_types=1);

namespace App\Traits\Model;

use Illuminate\Support\Str;

trait UuidTrait
{
    protected static function bootUuIdTrait(): void
    {
        static::creating(static function (self $model): void {
            $model->uuid = Str::uuid();
        });
    }
}
