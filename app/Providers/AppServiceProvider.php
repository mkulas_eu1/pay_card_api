<?php

namespace App\Providers;

use App\Services\Model\TransactionService;
use App\Services\Transaction\PayService;
use App\Services\Transaction\PayServiceContract;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function register()
    {
        //
    }

    public array $singletons = [
        PayServiceContract::class => PayService::class
    ];

    public function boot(): void
    {
        //
    }
}
