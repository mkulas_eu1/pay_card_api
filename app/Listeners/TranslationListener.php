<?php

namespace App\Listeners;

use App\Events\TranslationEvent;
use App\Http\Resources\Model\TransactionResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Mail;

class TranslationListener
{
    public function handle(TranslationEvent $event): void
    {
        Mail::send('emails.transaction',
            ['data' => new TransactionResource(
                $event->transaction)],
            function ($message) use ($event) {
                $message->to(Auth::user()['email'])
                    ->subject(Lang::get('message.transaction_message'));
            });
    }
}
