<?php declare(strict_types=1);

namespace App\Services\Model;

use App\Enum\StatusEnum;
use App\Models\CardPay;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class CardPayService
{
    public static function getCart(string $uuid): Model|Builder
    {
        return self::cardUuId($uuid)
            ->where('status', '!=', StatusEnum::DEACTIVATE);
    }


    public static function getCarts(): Builder
    {
        return self::cardUser()
            ->where('status', '!=', StatusEnum::DEACTIVATE);
    }


    public static function cardVerified(string $token): int
    {
        return self::cardToken($token)->update([
            'token' => null,
            'status' => StatusEnum::ACTIVE
        ]);
    }

    public static function cardDeleted(string $uuid): int
    {
        return self::cardUuId($uuid)->update([
            'status' => StatusEnum::DEACTIVATE,
            'deleted' => Carbon::now()
        ]);
    }


    public static function createCart(array $data): Model|Builder
    {
        return self::cardModel()->create([
            'user_id' => Auth::id(),
            'token' => Str::random(15 + strlen($data['card_name'])),
            'card_number' => generate_number(self::cardModel(), 'card_number', 0001000100010001, 9999999999999999),
            'card_name' => $data['card_name'],
            'cvv' => generate_number(self::cardModel(), 'cvv', 000, 999),
            'expire' => Carbon::now()->addYears(4),
            'money' => 00.00,
            'status' => StatusEnum::UNVERIFIED,
        ]);
    }


    public static function cardToken(string $token): Builder
    {
       return self::cardModel()
           ->where('token', $token);
    }


    public static function cardUuId(string $uuid, bool $forUser = true): Builder
    {
        if ($forUser === true) {
            $model = self::cardUser();
        } else{
            $model = self::cardModel();
        }

        return $model->where('uuid', $uuid);
    }


    public static function cardUser(): Builder
    {
        return self::cardModel()
            ->where('user_id',Auth::id());
    }


    public static function cardModel(): Builder
    {
        return CardPay::query();
    }
}