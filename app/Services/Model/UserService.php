<?php declare(strict_types=1);

namespace App\Services\Model;

use App\Models\User;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;

class UserService
{
    /**
     * @throws Exception
     */
    public static function register(array $data): Model|Builder
    {
        return self::user()->create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'pid' => generate_number(self::user(),'pid', 100000, 999999),
        ]);
    }

    public static function user(): Builder
    {
        return User::query();
    }
}