<?php

namespace App\Services\Model;

use App\Models\Transaction;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class TransactionService
{
    public static function transactionUser(): Builder
    {
        return self::transactionModel()
            ->where('user_id', Auth::id());
    }


    public static function transactionDetail(string $uuid): Builder
    {
        return self::transactionUser()
            ->where('uuid', $uuid);
    }


    public static function pay(array $data, string $typ): Model|Builder
    {
        return self::transactionModel()->create([
            'user_id' => $data['user'] ?? null,
            'from_card_id' => $data['from'],
            'to_card_id' => $data['to'],
            'amount' => $data['amount'],
            'typ' => $typ
        ]);
    }


    public static function transactionModel(): Builder
    {
        return Transaction::query();
    }
}