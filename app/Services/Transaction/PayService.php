<?php

namespace App\Services\Transaction;

use App\Services\Model\CardPayService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;

class PayService implements PayServiceContract
{
    public function cardToCard(array $data): array|bool
    {
        $from = $this->fromCard($data['from']);
        $to = $this->toCard($data['to']);

        $from->money = $this->decrypt($from->money) - $data['amount'];
        $to->money = $this->decrypt($to->money) + $data['amount'];

        $from->save();
        $to->save();

        if ($from->save() && $to->save()){
            return [
                'from' => $from,
                'to' => $to
            ];
        }

        return false;
    }


    private function fromCard($uuid): Model|Builder|null
    {
        return CardPayService::cardUuId($uuid)->first();
    }


    private function toCard($uuid): Model|Builder|null
    {
        return CardPayService::cardUuId($uuid, false)->first();
    }


    private function decrypt($num)
    {
        return Crypt::decrypt($num);
    }
}