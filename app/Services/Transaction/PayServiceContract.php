<?php

namespace App\Services\Transaction;

interface PayServiceContract
{
    public function cardToCard(array $data): array|bool;
}