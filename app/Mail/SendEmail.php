<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendEmail extends Mailable
{
    use Queueable, SerializesModels;

    private array $mailData;

    public function __construct($mailData)
    {
        $this->mailData = $mailData;
    }


    public function build(): SendEmail
    {
        return $this->view('emails.'.$this->mailData['template'],[
            'data' => $this->mailData
        ]);
    }
}
