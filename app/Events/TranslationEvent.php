<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * @property $transaction
 */
class TranslationEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public function __construct(array $transaction)
    {
        $this->transaction = $transaction;
    }
}
