<?php

use Illuminate\Database\Eloquent\Builder;

if (!function_exists('generate_number')) {
    /**
     * Generate unique code for model .
     * @example card_number , pid , cvv ...
     */
    function generate_number(Builder $model, string $column, int $min, int $max): int
    {
        $uniqueCode = false;
        $newCode = 0;
        while ($uniqueCode == false) {
            $newCode = mt_rand($min, $max);
            if ($model->where($column, $newCode)->exists())
                $uniqueCode = false;
            else
                $uniqueCode = true;
        }
        return $newCode;
    }
}