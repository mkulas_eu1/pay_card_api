<?php

namespace App\Models;

use App\Traits\Model\UuidTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property mixed $uuid
 */
class Transaction extends Model
{
    use HasFactory, UuidTrait;

    public function getRouteKeyName(): string
    {
        return 'uuid';
    }

    protected $hidden = ['id'];

    protected $fillable = [
        'uuid',
        'user_id',
        'from_card_id',
        'to_card_id',
        'amount',
        'typ',
    ];


    public function fromCard(): HasMany
    {
        return $this->hasMany(CardPay::class, 'id', 'from_card_id');
    }

    public function toCard(): HasMany
    {
        return $this->hasMany(CardPay::class, 'id', 'to_card_id');
    }
}
