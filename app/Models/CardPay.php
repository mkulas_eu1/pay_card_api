<?php

namespace App\Models;

use App\Traits\Model\UuidTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Crypt;

/**
 * @property mixed $card_number
 */
class CardPay extends Model
{
    use HasFactory, UuidTrait;

    public function getRouteKeyName(): string
    {
        return 'uuid';
    }

    // Auto encrypt data (decrypt in CardPayResource)
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->card_number = Crypt::encrypt($model->card_number);
            $model->cvv = Crypt::encrypt($model->cvv);
            $model->money = Crypt::encrypt($model->money);
            return $model;
        });

        self::updating(function ($model){
            $model->money = Crypt::encrypt($model->money);
        });
    }

    protected $hidden = ['id'];

    protected $fillable = [
        'uuid',
        'user_id',
        'token',
        'card_number',
        'card_name',
        'cvv',
        'expire',
        'money',
        'status',
        'deleted'
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
