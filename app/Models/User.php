<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Traits\Model\UuidTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable implements JWTSubject
{
    use HasApiTokens, HasFactory, Notifiable, UuidTrait;

    public function getRouteKeyName(): string
    {
        return 'uuid';
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims(): array
    {
        return [];
    }

    protected $hidden = [
        'id',
        'password',
    ];

    protected $fillable = [
        'uuid',
        'name',
        'email',
        'pid',
        'password',
    ];


    public function cards(): HasMany
    {
        return $this->hasMany(CardPay::class);
    }
}
