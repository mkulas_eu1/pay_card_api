<?php

namespace App\Enum;

enum TypPaymentEnum
{
    const CREDIT = 'credit';
    const PAYMENT = 'payment';
}