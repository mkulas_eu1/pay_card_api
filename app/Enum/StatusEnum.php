<?php

namespace App\Enum;

enum StatusEnum
{
    const ACTIVE = 'active'; // after verify
    const INACTIVE = 'inactive'; // if not used for a long time (years)
    const DEACTIVATE = 'deactivate'; // if expire
    const UNVERIFIED = 'unverified'; // after registration
    const BLOCKED = 'blocked'; // if blocked
}