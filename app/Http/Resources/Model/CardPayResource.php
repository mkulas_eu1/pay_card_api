<?php

namespace App\Http\Resources\Model;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Crypt;
use JetBrains\PhpStorm\ArrayShape;

/**
 * @property mixed $uuid
 * @property mixed $card_number
 * @property mixed $card_name
 * @property mixed $cvv
 * @property mixed $expire
 * @property mixed $money
 * @property mixed $status
 * @property mixed $created_at
 * @property mixed $user
 */
class CardPayResource extends JsonResource
{
    #[ArrayShape(['uuid' => "mixed", 'card_number' => "string", 'card_name' => "mixed", 'cvv' => "mixed", 'expire' => "string", 'money' => "mixed", 'created_at' => "string", 'status' => "mixed", 'user' => "UserResource"])]
    public function toArray($request): array
    {
        return [
            'uuid' => $this->uuid,
            'card_number' => join('-', str_split(
                Crypt::decrypt(
                    $this->card_number
                ), 4 )
            ),
            'card_name' => $this->card_name,
            'cvv' => Crypt::decrypt($this->cvv),
            'expire' => Carbon::parse($this->expire)
                ->format('m-y'),
            'money' => Crypt::decrypt($this->money),
            'created_at' => Carbon::parse($this->created_at)
                ->format('d-M-y'),
            'status' => $this->status,
            'user' => new UserResource($this->user)
        ];
    }
}
