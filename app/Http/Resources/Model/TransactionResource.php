<?php

namespace App\Http\Resources\Model;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use JetBrains\PhpStorm\ArrayShape;

/**
 * @property mixed $amount
 * @property mixed $created_at
 * @property mixed $toCard
 * @property mixed $fromCard
 * @property mixed $uuid
 * @property mixed $typ
 */
class TransactionResource extends JsonResource
{
    #[ArrayShape(['uuid' => "mixed", 'amount' => "mixed", 'to' => "\Illuminate\Http\Resources\Json\AnonymousResourceCollection", 'from' => "\Illuminate\Http\Resources\Json\AnonymousResourceCollection", 'time' => "string", 'typ' => "mixed"])]
    public function toArray($request): array
    {
        return [
            'uuid' => $this->uuid,
            'amount' => $this->amount,
            'to' =>  CardPayResource::collection($this->toCard),
            'from' =>  CardPayResource::collection($this->fromCard),
            'time' => Carbon::parse($this->created_at)->format('d-m-y'),
            'typ' => $this->typ
        ];
    }
}
