<?php

namespace App\Http\Resources\Model;

use Illuminate\Http\Resources\Json\JsonResource;
use JetBrains\PhpStorm\ArrayShape;

/**
 * @property mixed $uuid
 * @property mixed $pid
 * @property mixed $name
 * @property mixed $email
 */
class UserResource extends JsonResource
{
    #[ArrayShape(['uuid' => "mixed", 'pid' => "mixed", 'name' => "mixed", 'email' => "mixed"])]
    public function toArray($request): array
    {
        return [
            'uuid' => $this->uuid,
            'pid' => $this->pid,
            'name' => $this->name,
            'email' => $this->email,
        ];
    }
}
