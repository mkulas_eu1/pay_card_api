<?php

namespace App\Http\Resources;

use App\Http\Resources\Model\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Lang;
use JetBrains\PhpStorm\ArrayShape;

class TokenResource extends JsonResource
{
    #[ArrayShape(['user' => "\App\Http\Resources\UserResource", 'login_message' => "mixed", 'access_token' => "mixed", 'token_type' => "string", 'expires_in' => "float|int"])]
    public function toArray($request): array
    {
        return [
            'user' => new UserResource(auth()->user()),
            'login_message' => Lang::get('auth.login'),
            'access_token' => $this['token'],
            'token_type' => 'bearer',
            'expires_in' => auth()->factory(null)->getTTL() * 60,
        ];
    }
}
