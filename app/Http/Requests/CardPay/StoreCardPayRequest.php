<?php

namespace App\Http\Requests\CardPay;

use App\Models\CardPay;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Validation\ValidationException;
use JetBrains\PhpStorm\ArrayShape;

class StoreCardPayRequest extends FormRequest
{
    public function authorize(): bool
    {
        return Auth::check();
    }


    #[ArrayShape(['card_name' => "string[]"])]
    public function rules(): array
    {
        return [
            'card_name' => ['required','string','unique:' . ( new CardPay())->getTable() . ',card_name'],
        ];
    }


    #[ArrayShape(['card_name.required' => "mixed", 'card_name.string' => "mixed", 'card_name.unique' => "mixed"])]
    public function messages(): array
    {
        return [
            'card_name.required' => Lang::get('validate.required'),
            'card_name.string' => Lang::get('validate.string'),
            'card_name.unique' => Lang::get('validate.unique')
        ];
    }


    protected function failedValidation(Validator $validator)
    {
        $response = new Response(['error' => $validator->errors()->first()], 422);
        throw new ValidationException($validator, $response);
    }
}
