<?php

namespace App\Http\Requests\CardPay;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use JetBrains\PhpStorm\ArrayShape;

class UpdateCardPayRequest extends FormRequest
{
    public function authorize(): bool
    {
        return Auth::check();
    }

    #[ArrayShape(['money' => "string[]"])]
    public function rules(): array
    {
        return [
            'money' => ['integer','required']
        ];
    }


    protected function failedValidation(Validator $validator)
    {
        $response = new Response(['error' => $validator->errors()->first()], 422);
        throw new ValidationException($validator, $response);
    }
}
