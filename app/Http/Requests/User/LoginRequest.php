<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use JetBrains\PhpStorm\ArrayShape;

/**
 * @property mixed $password
 */
class LoginRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    #[ArrayShape(['pid' => "string[]", 'password' => "string[]"])]
    public function rules(): array
    {
        return [
            'pid' => ['required','integer'],
            'password' => ['required','string'],
        ];
    }
}
