<?php

namespace App\Http\Requests\User;

use App\Models\User;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;
use Illuminate\Validation\ValidationException;
use JetBrains\PhpStorm\ArrayShape;
use Illuminate\Http\Response;

class RegisterRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }


    #[ArrayShape(['name' => "string[]", 'email' => "string[]", 'password' => "string[]"])]
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'email' => ['required', 'email', 'unique:' . (new User())->getTable() . ',email'],
            'password' => ['required', 'string'],
        ];
    }


    #[ArrayShape(['email.unique' => "mixed", 'email.email' => "mixed", 'email.required' => "mixed", 'name.string' => "mixed", 'name.required' => "mixed", 'password.required' => "mixed", 'password.string' => "mixed"])]
    public function messages(): array
    {
        return [
            'email.unique' => Lang::get('validate.unique'),
            'email.email' => Lang::get('validate.email'),
            'email.required' => Lang::get('validate.required'),
            'name.string' => Lang::get('validate.string'),
            'name.required' => Lang::get('validate.required'),
            'password.required' => Lang::get('validate.required'),
            'password.string' => Lang::get('validate.string'),
        ];
    }


    protected function failedValidation(Validator $validator)
    {
        $response = new Response(['error' => $validator->errors()->first()], 422);
        throw new ValidationException($validator, $response);
    }
}
