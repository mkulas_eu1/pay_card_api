<?php

namespace App\Http\Requests\Transaction;

use App\Rules\Amount;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use JetBrains\PhpStorm\ArrayShape;

/**
 * @property mixed $from
 */
class TransactionRequest extends FormRequest
{
    public function authorize(): bool
    {
        return Auth::check();
    }

    #[ArrayShape(['from' => "string[]", 'to' => "string[]", 'amount' => "string[]", 'typ' => "string[]"])]
    public function rules(): array
    {
        return [
            'from' => ['required', 'string'],
            'to' => ['required', 'string','different:from'],
            'amount' => ['required', 'numeric',new Amount($this->from)],
        ];
    }


    protected function failedValidation(Validator $validator)
    {
        $response = new Response(['error' => $validator->errors()->first()], 422);
        throw new ValidationException($validator, $response);
    }
}
