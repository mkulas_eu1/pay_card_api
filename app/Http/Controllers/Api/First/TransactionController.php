<?php

namespace App\Http\Controllers\Api\First;

use App\Enum\TypPaymentEnum;
use App\Events\TranslationEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\Transaction\TransactionAddCreditRequest;
use App\Http\Requests\Transaction\TransactionRequest;
use App\Http\Resources\Model\CardPayResource;
use App\Http\Resources\Model\TransactionResource;
use App\Models\Transaction;
use App\Services\Model\CardPayService;
use App\Services\Model\TransactionService;
use App\Services\Transaction\PayServiceContract;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

class TransactionController extends Controller
{
    public function __construct(
        private readonly PayServiceContract $payService
    ){}

    public function index(): AnonymousResourceCollection
    {
        return TransactionResource::collection(
            TransactionService::transactionUser()
                ->where('created_at', '>', now()->subDays(30)
                    ->endOfDay())
                ->get()
        );
    }


    public function store(TransactionRequest $request): CardPayResource
    {
        $data = $request->validated();

        $cards = $this->payService->cardToCard($data);

        TransactionService::pay([
            'user' => Auth::id(),
            'from' => $cards['from']->id,
            'to' => $cards['to']->id,
            'amount' => $data['amount'],
        ], TypPaymentEnum::PAYMENT);

        TransactionService::pay([
            'from' => $cards['to']->id,
            'to' => $cards['from']->id,
            'amount' => $data['amount']
        ], TypPaymentEnum::CREDIT);

        TranslationEvent::dispatch($cards['from']->toArray());

        return new CardPayResource($cards['from']);
    }


    public function show(Transaction $transaction): TransactionResource
    {
        return new TransactionResource(
            TransactionService::transactionDetail($transaction->uuid)->first()
        );
    }


    public function addCredit(TransactionAddCreditRequest $request): CardPayResource
    {
        $data = $request->validated();

        $card = CardPayService::cardUuId($request->uuid)->first();

        $card->money = Crypt::decrypt($card->money) + $data['amount'];
        $card->save();

        TransactionService::pay([
            'user' => Auth::id(),
            'from' => $card->id,
            'to' => null,
            'amount' => $data['amount']
        ], TypPaymentEnum::CREDIT);

        return new CardPayResource($card);
    }
}
