<?php

namespace App\Http\Controllers\Api\First;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\LoginRequest;
use App\Http\Requests\User\RegisterRequest;
use App\Http\Resources\Model\UserResource;
use App\Http\Resources\TokenResource;
use App\Jobs\SendEmailJob;
use App\Services\Model\UserService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Lang;

class AuthController extends Controller
{
    public function __construct()
    {
       $this->middleware('auth:api')->except(['login', 'register']);
    }


    public function refresh(): TokenResource
    {
        return new TokenResource(
            ['token' => auth()->refresh()]
        );
    }


    /**
     * @throws Exception
     */
    public function register(RegisterRequest $request): UserResource
    {
        $register = UserService::register($request->validated());

        dispatch(new SendEmailJob([
            'template' => 'register',
            'email' => $register->email,
            'name' => $register->name,
            'pid' => $register->pid
        ]));

        return new UserResource($register);
    }


    public function login(LoginRequest $request): JsonResponse|TokenResource
    {
        $data = $request->validated();

        if (! $token = auth()->attempt([
            'pid' => $data['pid'],
            'password' => $data['password']
        ])) {
            return response()->json(
                ['error' => Lang::get('message.unauthorized')],
                401
            );
        }

        return new TokenResource(
            ['token' => $token]
        );
    }


    /**
     * @return JsonResponse
     */
    public function logout(): JsonResponse
    {
        auth()->logout();
        return response()->json(
            ['message' => Lang::get('message.success_logout')]
        );
    }
}