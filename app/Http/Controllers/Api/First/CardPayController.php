<?php

namespace App\Http\Controllers\Api\First;

use App\Http\Controllers\Controller;
use App\Http\Requests\CardPay\StoreCardPayRequest;
use App\Http\Resources\Model\CardPayResource;
use App\Jobs\SendEmailJob;
use App\Services\Model\CardPayService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;

class CardPayController extends Controller
{
    public function index(): AnonymousResourceCollection
    {
        return CardPayResource::collection(
            CardPayService::getCarts()
                ->paginate(2)
        );
    }


    public function store(StoreCardPayRequest $request): CardPayResource
    {
        $card = CardPayService::createCart($request->validated());

        dispatch(new SendEmailJob([
            'email' => Auth::user()['email'],
            'template' => 'card_create',
            'card_name' => $card->card_name,
            'token' => $card->token,
        ]));

        return new CardPayResource($card);
    }


    public function show($cardPay): CardPayResource
    {
        return new CardPayResource(
            CardPayService::getCart($cardPay)
                ->first()
        );
    }


    public function destroy($cardPay): JsonResponse
    {
        CardPayService::cardDeleted($cardPay);

        return response()->json(
            ['message' => Lang::get('message.card_remove')]
        );
    }


    public function verify(Request $request): JsonResponse
    {
        CardPayService::cardVerified($request->token);

        return response()->json(
            ['message' => Lang::get('message.card_verify')]
        );
    }
}
