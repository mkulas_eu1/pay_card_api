Implementácia https://jwt-auth.readthedocs.io/en/develop/laravel-installation/

        composer require tomfordrumm/jwt-auth:dev-develop

UuidTrait ( pre väčšiu bezpečnosť údajov )

        mám vo zvyku ponechať aj id s ktorým pracujem na backende na frontend vraciam uuid
        pre väčšiu bezpečnosť . 

Services -> Model 

        používam tento typ services aby som mal všetky query po kope . Uľahčuje to refactoring ,
        zmeny v db , zlepšuje prehľad aplikácie , každá metóda obsahuje len jednu časť . Ktorá sa nabaľuje 
        podľa potreby . Vždy nabaľujem smerom hore . ( metóda ktorá inicializuje model je úplne dole a 
        následná metóda ktorá sa nabaľuje je nad ňou ) . V pripade service ktora je urcena pre logiku 
        vytvaram aj contractor . 

Services a interface

        pouzivam pri logikach inych ako modelovych .

Enum 

        zatial ho pouzivam pre definovanie typov , rozne stringy alebo nieco co sa pouziva napriec app,
        v tomto kode pouzivam enum ako definiciu hodnot pre enum v db nasledne pri meneni zaznamu v db 
        na dany string 

Helpers 

        funkcie ktore pouzivam na viacerych miestach . V tomto pripade je pouzita generate_number pre 
        unikatne generovanie cisiel napriklad pri cvv , card_number, ... 

Encryption

        Prebieha v modeli ako boot metoda ( CardPay ) pred vytvorenim nasledne decryptujem v resource ( CardPayResource ) . Ak by sa udaje
        vratili inak ako cez resource  vysli by cryptovane . Zvecsi to security .

Rule  - custom validacia 

        kontrola ci sa na karte nachadzaju dostatocne prostriedky pre platbu 

Pouzity job ( odosle mail po registracii )

Job na odoslanie mailu pre aktivaciu karty 

Event a listener odosle mail po odoslani platby na inu kartu 



