<?php

use App\Enum\StatusEnum;
use App\Models\CardPay;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create((new CardPay())->getTable(), function (Blueprint $table) {
            $table->id();
            $table->uuid();
            $table->foreignId('user_id')->constrained((new User())->getTable());
            $table->string('token')->nullable();
            $table->string('card_number');
            $table->string('card_name');
            $table->string('cvv');
            $table->date('expire');
            $table->string('money')->nullable()->default(0);
            $table->enum('status',[
                StatusEnum::ACTIVE,
                StatusEnum::DEACTIVATE,
                StatusEnum::INACTIVE,
                StatusEnum::UNVERIFIED,
                StatusEnum::BLOCKED
            ]);
            $table->timestamps();
            $table->date('deleted')->nullable();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists((new CardPay())->getTable());
    }
};
