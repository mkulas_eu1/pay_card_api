<?php

use App\Enum\TypPaymentEnum;
use App\Models\CardPay;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create((new Transaction())->getTable(), function (Blueprint $table) {
            $table->id();
            $table->uuid();
            $table->foreignId('user_id')->nullable()->constrained((new User())->getTable());
            $table->foreignId('from_card_id')->nullable()->constrained((new CardPay())->getTable());
            $table->foreignId('to_card_id')->nullable()->constrained((new CardPay())->getTable());
            $table->string('amount');
            $table->enum('typ',[
                TypPaymentEnum::CREDIT,
                TypPaymentEnum::PAYMENT])->default(null);
            $table->timestamps();
        });
    }


    public function down(): void
    {
        Schema::dropIfExists((new Transaction())->getTable());
    }
};
