Popis : api auth cez jwt 
- generovanie unikatneho cisla pre platobnu kartu 
- transakcie medzi platobnymi kartami
- pridavanie kreditu
- verifikacia platobnej karty emailom
- automaticke akcie pri tvorbe zaznamu / uuid
... 

Start project:

    php8.1 , composer 

    composer install
    cp .env.example .env

    Config DB & SMTP in ENV

    php artisan jwt:secret

    php artisan serve

Run queue work

    php artisan queue:work
